import Control.Monad
import S3Bits
import Data.Word
import Data.Bits
import System.Environment

-- writeBits16 :: (Integral a, Bits a) => a -> String
-- writeBits16 x = concatMap (show.toInteger.((.&.) 1).rotateR x) [15,14..0]

main = do
  args <- getArgs
  conf <- readFile (args !! 0)
  frames <- readFile (args !! 1)
  r <- return $ codeGenAct $ fillExtFrames (read conf) (read frames)
  mapM (putStrLn.((++) "00").writeBits16) r
  replicateM_ (3072 - length r) (putStrLn "010010000000000000")
