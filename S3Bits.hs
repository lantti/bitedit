module S3Bits where

import Data.List
import Data.Maybe
import Data.Word
import Data.Bits
import Data.Tuple

getCode :: (Enum a, Integral b) => a -> b
getCode = fromIntegral.fromEnum

getName :: (Integral a, Enum b) => a -> b
getName = toEnum.fromIntegral

writeBits16 :: Word16 -> String
writeBits16 x = concatMap (show.((.&.) 1).rotateR x) [15,14..0]

readBits16 :: String -> Word16
readBits16 = foldl' (\a x -> (shiftL a 1) .|. (read [x])) 0

writeRbt :: String -> String -> String -> String -> [Word16] -> String
writeRbt name arch part date xs = 
  let header =  "Xilinx ASCII Bitstream\n" ++
                "Created by Bitstream P.20131013\n" ++
                "Design name: \t" ++ name ++ ";UserID=0xFFFFFFFF\n" ++
                "Architecture:\t" ++ arch ++ "\n" ++
                "Part:        \t" ++ part ++ "\n" ++
                "Date:        \t" ++ date ++ "\n" ++
                "Bits:        \t" ++ (show ((length xs) * 16)) ++ "\n"
      body = unlines (map writeBits16 xs)
  in header ++ body

readRbt :: String -> [Word16]
readRbt rbt =
  let bits = lines rbt
      body = dropWhile (any (\c -> not ((c == '0') || (c == '1')))) bits
  in map readBits16 body

frameOrder :: [Word32]
frameOrder =
  [0x00000000,0x00000001,

   0x00010000,0x00010001,

   0x00020000,0x00020001,0x00020002,0x00020003,0x00020004,0x00020005,0x00020006,0x00020007,
   0x00020008,0x00020009,0x0002000a,0x0002000b,0x0002000c,0x0002000d,0x0002000e,0x0002000f,
   0x00020010,0x00020011,0x00020012,

   0x00030000,0x00030001,0x00030002,0x00030003,0x00030004,0x00030005,0x00030006,0x00030007,
   0x00030008,0x00030009,0x0003000a,0x0003000b,0x0003000c,0x0003000d,0x0003000e,0x0003000f,
   0x00030010,0x00030011,0x00030012,

   0x00040000,0x00040001,0x00040002,0x00040003,0x00040004,0x00040005,0x00040006,0x00040007,
   0x00040008,0x00040009,0x0004000a,0x0004000b,0x0004000c,0x0004000d,0x0004000e,0x0004000f,
   0x00040010,0x00040011,0x00040012,

   0x00050000,0x00050001,0x00050002,0x00050003,0x00050004,0x00050005,0x00050006,0x00050007,
   0x00050008,0x00050009,0x0005000a,0x0005000b,0x0005000c,0x0005000d,0x0005000e,0x0005000f,
   0x00050010,0x00050011,0x00050012,

   0x00060000,0x00060001,0x00060002,0x00060003,0x00060004,0x00060005,0x00060006,0x00060007,
   0x00060008,0x00060009,0x0006000a,0x0006000b,0x0006000c,0x0006000d,0x0006000e,0x0006000f,
   0x00060010,0x00060011,0x00060012,

   0x00070000,0x00070001,0x00070002,0x00070003,0x00070004,0x00070005,0x00070006,0x00070007,
   0x00070008,0x00070009,0x0007000a,0x0007000b,0x0007000c,0x0007000d,0x0007000e,0x0007000f,
   0x00070010,0x00070011,0x00070012,

   0x00080000,0x00080001,0x00080002,0x00080003,0x00080004,0x00080005,0x00080006,0x00080007,
   0x00080008,0x00080009,0x0008000a,0x0008000b,0x0008000c,0x0008000d,0x0008000e,0x0008000f,
   0x00080010,0x00080011,0x00080012,

   0x00090000,0x00090001,0x00090002,0x00090003,0x00090004,0x00090005,0x00090006,0x00090007,
   0x00090008,0x00090009,0x0009000a,0x0009000b,0x0009000c,0x0009000d,0x0009000e,0x0009000f,
   0x00090010,0x00090011,0x00090012,

   0x000a0000,0x000a0001,0x000a0002,0x000a0003,0x000a0004,0x000a0005,0x000a0006,0x000a0007,
   0x000a0008,0x000a0009,0x000a000a,0x000a000b,0x000a000c,0x000a000d,0x000a000e,0x000a000f,
   0x000a0010,0x000a0011,0x000a0012,

   0x000b0000,0x000b0001,0x000b0002,0x000b0003,0x000b0004,0x000b0005,0x000b0006,0x000b0007,
   0x000b0008,0x000b0009,0x000b000a,0x000b000b,0x000b000c,0x000b000d,0x000b000e,0x000b000f,
   0x000b0010,0x000b0011,0x000b0012,

   0x000c0000,0x000c0001,0x000c0002,0x000c0003,0x000c0004,0x000c0005,0x000c0006,0x000c0007,
   0x000c0008,0x000c0009,0x000c000a,0x000c000b,0x000c000c,0x000c000d,0x000c000e,0x000c000f,
   0x000c0010,0x000c0011,0x000c0012,

   0x000d0000,0x000d0001,0x000d0002,0x000d0003,0x000d0004,0x000d0005,0x000d0006,0x000d0007,
   0x000d0008,0x000d0009,0x000d000a,0x000d000b,0x000d000c,0x000d000d,0x000d000e,0x000d000f,
   0x000d0010,0x000d0011,0x000d0012,

   0x000e0000,0x000e0001,0x000e0002,0x000e0003,0x000e0004,0x000e0005,0x000e0006,0x000e0007,
   0x000e0008,0x000e0009,0x000e000a,0x000e000b,0x000e000c,0x000e000d,0x000e000e,0x000e000f,
   0x000e0010,0x000e0011,0x000e0012,

   0x000f0000,0x000f0001,0x000f0002,0x000f0003,0x000f0004,0x000f0005,0x000f0006,0x000f0007,
   0x000f0008,0x000f0009,0x000f000a,0x000f000b,0x000f000c,0x000f000d,0x000f000e,0x000f000f,
   0x000f0010,0x000f0011,0x000f0012,

   0x00100000,0x00100001,

   0x04000000,0x04000001,0x04000002,0x04000003,0x04000004,0x04000005,0x04000006,0x04000007,
   0x04000008,0x04000009,0x0400000a,0x0400000b,0x0400000c,0x0400000d,0x0400000e,0x0400000f,
   0x04000010,0x04000011,0x04000012,0x04000013,0x04000014,0x04000015,0x04000016,0x04000017,
   0x04000018,0x04000019,0x0400001a,0x0400001b,0x0400001c,0x0400001d,0x0400001e,0x0400001f,
   0x04000020,0x04000021,0x04000022,0x04000023,0x04000024,0x04000025,0x04000026,0x04000027,
   0x04000028,0x04000029,0x0400002a,0x0400002b,0x0400002c,0x0400002d,0x0400002e,0x0400002f,
   0x04000030,0x04000031,0x04000032,0x04000033,0x04000034,0x04000035,0x04000036,0x04000037,
   0x04000038,0x04000039,0x0400003a,0x0400003b,0x0400003c,0x0400003d,0x0400003e,0x0400003f,
   0x04000040,0x04000041,0x04000042,0x04000043,0x04000044,0x04000045,0x04000046,0x04000047,
   0x04000048,0x04000049,0x0400004a,0x0400004b,

   0x08000000,0x08000001,0x08000002,0x08000003,0x08000004,0x08000005,0x08000006,0x08000007,
   0x08000008,0x08000009,0x0800000a,0x0800000b,0x0800000c,0x0800000d,0x0800000e,0x0800000f,
   0x08000010,0x08000011,0x08000012]

emptyFrame = replicate 74 0

data Op = Noop | Read | Write deriving (Read, Show, Eq, Enum)

data Reg = Crc | FarMaj | FarMin | Fdri | Fdro | Cmd | Ctl | Mask | Stat | Lout | Cor1 | Cor2 | PwrdnReg
         | Flr | Idcode | Snowplow | HcOptReg | ReservedReg | Csbo | General1 | General2 | ModeReg | PuGwe | PuGts | MfwrReg
         | CclkFreq | SeuOpt | ExpSign | RdbkSign
  deriving (Read, Show, Eq, Enum)


data Packet = Packet1 Op Reg [Word16] | Packet2 Op Reg [Word16] deriving (Read, Show, Eq)
packetCode :: Packet -> [Word16]
packetCode (Packet1 op reg datas) = ((bit 13) .|.
                                     (shiftL (getCode op) 11) .|.
                                     (shiftL (getCode reg) 5) .|.
                                     (mod (fromIntegral $ length datas) 32))
                                    :datas
packetCode (Packet2 op reg datas) = ((bit 14) .|. 
                                     (shiftL (getCode op) 11) .|.
                                     (shiftL (getCode reg) 5))
                                    :(shiftR (fromIntegral $ length datas) 16)
                                    :(fromIntegral $ length datas)
                                    :datas


codePacket :: [Word16] -> (Maybe Packet, [Word16])
codePacket [] = (Nothing, [])
codePacket (header:body) = 
  let ptyp = (shiftR header 13)
      op = getName $ (shiftR header 11) .&. 0x0003
      reg = getName $ (shiftR header 5) .&. 0x003F
  in case (ptyp,body) of 
    (1,b) -> let wc = fromIntegral $ header .&. 0x001F
                 (this,next) = splitAt wc b
             in (Just (Packet1 op reg this), next)
    (2,b1:b2:b) -> let wc = (shiftL (fromIntegral b1) 16) .|. (fromIntegral b2)
                       (this,next) = splitAt wc b
                   in (Just (Packet2 op reg this), next)
    _ -> (Nothing, header:body)

packets :: [Word16] -> [Packet]
packets = 
  let packets' x =
        let (pkt,rest) = codePacket x
        in case pkt of
             Nothing -> []
             Just p  -> p:(packets' rest)
  in packets'.tail.(dropWhile (/= 0xAA99))

data Cmd = Null | Wcfg | Mfwr | Lfrm | Rcfg | Start | Rcap | Rcrc | AGhigh | ReservedCmd1| Grestore | Shutdown | Gcapture | Desync | Reboot | ReservedCmd2 deriving (Read, Show, Eq, Enum)


data EnMboot = DisMboot | EnMboot deriving (Read, Show, Eq, Enum)
data Sbits = ReadWrite | IcapOnly | CrcOnly deriving (Read, Show, Eq, Enum)
data Persist = NoPersist | Persist deriving (Read, Show, Eq, Enum)
data Icap = NoIcap | Icap deriving (Read, Show, Eq, Enum)
data GtsUserB = IoHighZ | IoActive deriving (Read, Show, Eq, Enum)
ctlCode :: EnMboot -> Sbits -> Persist -> Icap -> GtsUserB -> Word16
ctlCode enmboot sbits persist icap gtsuserb = 
  (shiftL (getCode enmboot) 7) .|.
  (shiftL (getCode sbits) 4) .|.
  (shiftL (getCode persist) 3) .|.
  (shiftL (getCode icap) 2) .|.
  (getCode gtsuserb)

codeCtl :: Word16 -> (EnMboot,Sbits,Persist,Icap,GtsUserB)
codeCtl w = (getName $ shiftR w 7 .&. 0x0001,
             getName $ shiftR w 4 .&. 0x0003,
             getName $ shiftR w 3 .&. 0x0001,
             getName $ shiftR w 2 .&. 0x0001,
             getName $ w .&. 0x0001)


data MaskEnMboot = MaskEnMboot | UnMaskEnMboot deriving (Read, Show, Eq, Enum)
data MaskSbits = MaskSbits | UnMaskSbits deriving (Read, Show, Eq, Enum)
data MaskPersist = MaskPersist | UnMaskPersist deriving (Read, Show, Eq, Enum)
data MaskIcap = MaskIcap | UnMaskIcap deriving (Read, Show, Eq, Enum)
data MaskGtsUserB = MaskGtsUserB | UnMaskGtsUserB deriving (Read, Show, Eq, Enum)
maskCode :: MaskEnMboot -> MaskSbits -> MaskPersist -> MaskIcap -> MaskGtsUserB -> Word16
maskCode maskenmboot masksbits maskpersist maskicap maskgtsuserb =
  (shiftL (getCode maskenmboot) 7) .|.
  (shiftL (getCode masksbits) 5) .|.
  (shiftL (getCode masksbits) 4) .|.
  (shiftL (getCode maskpersist) 3) .|.
  (shiftL (getCode maskicap) 2) .|.
  (getCode maskgtsuserb)

codeMask :: Word16 -> (MaskEnMboot,MaskSbits,MaskPersist,MaskIcap,MaskGtsUserB)
codeMask w = (getName $ shiftR w 7 .&. 0x0001,
              getName $ shiftR w 5 .&. shiftR w 4 .&. 0x0001,
              getName $ shiftR w 3 .&. 0x0001,
              getName $ shiftR w 2 .&. 0x0001,
              getName $ w .&. 0x0001)


data DriveAwake = OpenDrainAwake | DriveAwake deriving (Read, Show, Eq, Enum)
data CrcBypass = CrcEnabled | CrcDisabled deriving (Read, Show, Eq, Enum)
data DonePipe = NoDonePipe | DonePipe deriving (Read, Show, Eq, Enum)
data DriveDone = OpenDrainDone | DriveDone  deriving (Read, Show, Eq, Enum)
data SsClkSrc = Cclk | UserClk | JtagClk deriving (Read, Show, Eq, Enum)
cor1Code :: DriveAwake -> CrcBypass -> DonePipe -> DriveDone -> SsClkSrc -> Word16
cor1Code driveawake crcbypass donepipe drivedone ssclksrc =
  (shiftL (getCode driveawake) 15) .|.
  0x2F00 .|.
  (shiftL (getCode crcbypass) 4) .|.
  (shiftL (getCode donepipe) 3) .|.
  (shiftL (getCode drivedone) 2) .|.
  (getCode ssclksrc)

codeCor1 :: Word16 -> (DriveAwake,CrcBypass,DonePipe,DriveDone,SsClkSrc)
codeCor1 w = (getName $ shiftR w 15 .&. 0x0001,
              getName $ shiftR w 4 .&. 0x0001,
              getName $ shiftR w 3 .&. 0x0001,
              getName $ shiftR w 2 .&. 0x0001,
              getName $ w .&. 0x0003)


data ResetOnErr = NoResetOnErr | ResetOnErr deriving (Read, Show, Eq, Enum)
data BpiDiv8 = NoBpiDiv8 | BpiDiv8 deriving (Read, Show, Eq, Enum)
data Single = RcapRepeat | RcapSingle deriving (Read, Show, Eq, Enum)
data DoneCycle = DoneCycle Int deriving (Read, Show)
data LockCycle = NoWait | LockCycle Int deriving (Read, Show)
data GtsCycle = GtsCycle Int deriving (Read, Show)
data GweCycle = GweCycle Int deriving (Read, Show)
cor2Code :: ResetOnErr -> BpiDiv8 -> Single -> DoneCycle -> LockCycle -> GtsCycle -> GweCycle -> Word16
cor2Code resetonerr bpidiv8 single (DoneCycle donecycle) lockcycle (GtsCycle gtscycle) (GweCycle gwecycle) =
  (shiftL (getCode resetonerr) 15) .|.
  (shiftL (getCode bpidiv8) 13) .|.
  (shiftL (getCode single) 12) .|.
  (case lockcycle of 
     NoWait -> shiftL 0x7 6
     LockCycle x -> fromIntegral $ shiftL (mod x 8) 6)
  .|. 
  (fromIntegral $ shiftL (mod donecycle 8) 9) .|.
  (fromIntegral $ shiftL (mod gtscycle 8) 3) .|.
  (fromIntegral $ mod gwecycle 8)

codeCor2 :: Word16 -> (ResetOnErr,BpiDiv8,Single,DoneCycle,LockCycle,GtsCycle,GweCycle)
codeCor2 w = (getName $ shiftR w 15 .&. 0x0001,
              getName $ shiftR w 13 .&. 0x0001,
              getName $ shiftR w 12 .&. 0x0001,
              DoneCycle (fromIntegral $ shiftR w 9 .&. 0x0007),
              (let lockcycle = shiftR w 6 .&. 0x0007
               in if (lockcycle == 0x7)
                  then NoWait
                  else LockCycle (fromIntegral lockcycle)),
              GtsCycle (fromIntegral $ shiftR w 3 .&. 0x0007),
              GweCycle (fromIntegral $ w .&. 0x0007))


data WakeDelay2 = WakeDelay2 Int deriving (Read, Show, Eq)
data WakeDelay1 = WakeDelay1 Int deriving (Read, Show, Eq)
data Filter = Filter | NoFilter deriving (Read, Show, Eq, Enum)
data EnPgsr = NoPulseGsr | PulseGsr deriving (Read, Show, Eq, Enum)
data EnPwrdn = NoSuspend | Suspend deriving (Read, Show, Eq, Enum)
data EnPor = Por | NoPor deriving (Read, Show, Eq, Enum)
data KeepSclk = Mcclk | SsClkSrc deriving (Read, Show, Eq, Enum)
pwrdnCode :: WakeDelay2 -> WakeDelay1 -> Filter -> EnPgsr -> EnPwrdn -> EnPor -> KeepSclk -> Word16
pwrdnCode (WakeDelay2 wakedelay2) (WakeDelay1 wakedelay1) filter enpgsr enpwrdn enpor keepsclk =
  (fromIntegral $ shiftL (mod wakedelay2 32) 9) .|.
  (fromIntegral $ shiftL (mod wakedelay1 8) 6) .|.
  (shiftL (getCode filter) 5) .|.
  (shiftL (getCode enpgsr) 4) .|.
  (shiftL (getCode enpwrdn) 2) .|.
  (shiftL (getCode enpor) 1) .|.
  (getCode keepsclk)

codePwrdn :: Word16 -> (WakeDelay2,WakeDelay1,Filter,EnPgsr,EnPwrdn,EnPor,KeepSclk)
codePwrdn w = (WakeDelay2 (fromIntegral $ shiftR w 9 .&. 0x001F),
               WakeDelay1 (fromIntegral $ shiftR w 6 .&. 0x0007),
               getName $ shiftR w 5 .&. 0x0001,
               getName $ shiftR w 4 .&. 0x0001,
               getName $ shiftR w 2 .&. 0x0001,
               getName $ shiftR w 1 .&. 0x0001,
               getName $ shiftR w 0 .&. 0x0001)


data BramSkip = NoBramSkip | BramSkip deriving (Read, Show, Eq, Enum)
data TwoRound = OneRound | TwoRound deriving (Read, Show, Eq, Enum)
data HcCycle = HcCycle Int deriving (Read, Show, Eq)
hcOptCode :: BramSkip -> TwoRound -> HcCycle -> Word16
hcOptCode bramskip tworound (HcCycle hccycle) = (shiftL (getCode bramskip) 5) .|. (shiftL (getCode tworound) 4) .|. (fromIntegral $ mod hccycle 16)

codeHcOpt :: Word16 -> (BramSkip,TwoRound,HcCycle)
codeHcOpt w = (getName $ shiftR w 5 .&. 0x0001,
               getName $ shiftR w 4 .&. 0x0001,
               HcCycle (fromIntegral $ w .&. 0x000F))

data NewMode = NoNewMode | NewMode deriving (Read, Show, Eq, Enum)
data Bootmode = SerialM | SpiM | BpiUp | InternalM | ReservedMode | Jtag | ParallelS | SerialS deriving (Read, Show, Eq, Enum)
modeCode :: NewMode -> Bootmode -> Word16
modeCode newmode bootmode = (shiftL (getCode newmode) 6) .|. 
                            (shiftL (getCode bootmode) 3) .|. 0x7 

codeMode :: Word16 -> (NewMode, Bootmode)
codeMode w = (getName $ shiftR w 6 .&. 0x0001,
              getName $ shiftR w 3 .&. 0x0007)


data Action = NoOperation
            | WriteCrc Word32 
            | ReadCrc
            | WriteFar Word32
            | WriteFarMaj Word16
            | WriteFarMin Word16
            | WriteFdri [Word16]
            | WriteFdriExt Word32 Word32
            | WriteSingleFramesExt Word32 Word32
            | WriteFdriNull Int
            | ReadFdro
            | WriteCmd Cmd
            | ReadCmd
            | WriteCtl EnMboot Sbits Persist Icap GtsUserB
            | ReadCtl
            | WriteMask MaskEnMboot MaskSbits MaskPersist MaskIcap MaskGtsUserB
            | ReadMask
            | ReadStat
            | WriteLout [Word16]
            | WriteCor1 DriveAwake CrcBypass DonePipe DriveDone SsClkSrc
            | ReadCor1
            | WriteCor2 ResetOnErr BpiDiv8 Single DoneCycle LockCycle GtsCycle GweCycle 
            | ReadCor2
            | WritePwrdnReg WakeDelay2 WakeDelay1 Filter EnPgsr EnPwrdn EnPor KeepSclk
            | ReadPwrdnReg
            | WriteFlr Word16
            | WriteIdcode Word32
            | ReadIdcode
            | WriteSnowplow Word16
            | WriteHcOptReg BramSkip TwoRound HcCycle
            | ReadHcOptReg
            | WriteCsbo
            | WriteGeneral1 Word16
            | ReadGeneral1
            | WriteGeneral2 Word16
            | ReadGeneral2
            | WriteModeReg NewMode Bootmode
            | ReadModeReg
            | WritePuGwe Word16
            | WritePuGts Word16
            | WriteMfwr
            | WriteCclkFreq Word16
            | WriteSeuOpt Word16
            | WriteExpSign Word32
            | ReadExpSign
            | ReadRdbkSign
  deriving (Read, Show)
actionPacket :: Action -> Packet
actionPacket NoOperation = Packet1 Noop Crc []
actionPacket (WriteCrc crc) = Packet1 Write Crc [fromIntegral $ shiftR crc 16, fromIntegral crc]
actionPacket ReadCrc = undefined
actionPacket (WriteFar addr) = Packet1 Write FarMaj [fromIntegral $ shiftR addr 16, fromIntegral addr]
actionPacket (WriteFarMaj addr) = Packet1 Write FarMaj [addr]
actionPacket (WriteFarMin addr) = Packet1 Write FarMin [addr]
actionPacket (WriteFdri datas) = Packet2 Write Fdri datas
actionPacket (WriteFdriExt _ _) = undefined
actionPacket (WriteSingleFramesExt _ _) = undefined
actionPacket (WriteFdriNull fl)= Packet2 Write Fdri (replicate fl 0)
actionPacket ReadFdro = undefined
actionPacket (WriteCmd cmd) = Packet1 Write Cmd [getCode cmd]
actionPacket ReadCmd = undefined
actionPacket (WriteCtl enmboot sbits persist icap gtsuserb) = Packet1 Write Ctl [ctlCode enmboot sbits persist icap gtsuserb]
actionPacket ReadCtl = undefined
actionPacket (WriteMask enmboot sbits persist icap gtsuserb) = Packet1 Write Mask [maskCode enmboot sbits persist icap gtsuserb]
actionPacket ReadMask = undefined
actionPacket ReadStat = undefined
actionPacket (WriteLout datas) = Packet1 Write Lout datas
actionPacket (WriteCor1 drvawake crcbypass donepipe drvdone ssclk) = Packet1 Write Cor1 [cor1Code drvawake crcbypass donepipe drvdone ssclk]
actionPacket ReadCor1 = undefined
actionPacket (WriteCor2 rsterr bpidiv single donec lockc gtsc gwec) = Packet1 Write Cor2 [cor2Code rsterr bpidiv single donec lockc gtsc gwec]
actionPacket ReadCor2 = undefined
actionPacket (WritePwrdnReg wdelay2 wdelay1 fltr pgsr pwrdn por sclk) = Packet1 Write PwrdnReg [pwrdnCode wdelay2 wdelay1 fltr pgsr pwrdn por sclk]
actionPacket ReadPwrdnReg = undefined
actionPacket (WriteFlr framelen) = Packet1 Write Flr [framelen]
actionPacket (WriteIdcode idcode) = Packet1 Write Idcode [fromIntegral $ shiftR idcode 16, fromIntegral idcode]
actionPacket ReadIdcode = undefined
actionPacket (WriteSnowplow addr) = Packet1 Write Snowplow [addr]
actionPacket (WriteHcOptReg bramskip tworound hccycle) = Packet1 Write HcOptReg [hcOptCode bramskip tworound hccycle]
actionPacket ReadHcOptReg = undefined
actionPacket WriteCsbo = undefined
actionPacket (WriteGeneral1 addr) = Packet1 Write General1 [addr]
actionPacket ReadGeneral1 = undefined
actionPacket (WriteGeneral2 addr) = Packet1 Write General2 [addr]
actionPacket ReadGeneral2 = undefined
actionPacket (WriteModeReg newmode bootmode) = Packet1 Write ModeReg [modeCode newmode bootmode]
actionPacket ReadModeReg = undefined
actionPacket (WritePuGwe cycle) = Packet1 Write PuGwe [cycle]
actionPacket (WritePuGts cycle) = Packet1 Write PuGts [cycle]
actionPacket WriteMfwr = Packet1 Write MfwrReg [0x0,0x0,0x0,0x0]
actionPacket (WriteCclkFreq freq) = Packet1 Write CclkFreq [freq]
actionPacket (WriteSeuOpt seuopt) = Packet1 Write SeuOpt [seuopt]
actionPacket (WriteExpSign expsign) = Packet1 Write ExpSign [fromIntegral $ shiftR expsign 16, fromIntegral expsign]
actionPacket ReadExpSign = undefined
actionPacket ReadRdbkSign = undefined

packetAction :: Packet -> Action
packetAction (Packet1 Noop _ _) = NoOperation
packetAction (Packet1 Write Crc (high:low:[])) = WriteCrc ((shiftL (fromIntegral high) 16) .|. (fromIntegral low))
packetAction (Packet1 Write FarMaj (high:low:[])) = WriteFar ((shiftL (fromIntegral high) 16) .|. (fromIntegral low))
packetAction (Packet1 Write FarMaj (high:[])) = WriteFarMaj high
packetAction (Packet1 Write FarMin (low:[])) = WriteFarMin low
packetAction (Packet2 Write Fdri datas) = WriteFdri datas
packetAction (Packet1 Write Cmd (cmd:[])) = WriteCmd (getName cmd) 
packetAction (Packet1 Write Ctl (ctl:[])) =
  let (enmboot,sbits,persist,icap,gtsuserb) = codeCtl ctl
  in WriteCtl enmboot sbits persist icap gtsuserb
packetAction (Packet1 Write Mask (mask:[])) =
  let (enmboot,sbits,persist,icap,gtsuserb) = codeMask mask
  in WriteMask enmboot sbits persist icap gtsuserb
packetAction (Packet1 Write Lout datas) = WriteLout datas
packetAction (Packet1 Write Cor1 (cor1:[])) =
  let (drvawake,crcbypass,donepipe,drvdone,ssclk) = codeCor1 cor1 
  in WriteCor1 drvawake crcbypass donepipe drvdone ssclk
packetAction (Packet1 Write Cor2 (cor2:[])) =
  let (rsterr,bpidiv,single,donec,lockc,gtsc,gwec) = codeCor2 cor2
  in WriteCor2 rsterr bpidiv single donec lockc gtsc gwec
packetAction (Packet1 Write PwrdnReg (pwrdnW:[])) =
  let (wdelay2,wdelay1,fltr,pgsr,pwrdn,por,sclk) = codePwrdn pwrdnW
  in WritePwrdnReg wdelay2 wdelay1 fltr pgsr pwrdn por sclk
packetAction (Packet1 Write Flr (framelen:[])) = WriteFlr framelen
packetAction (Packet1 Write Idcode (high:low:[])) = WriteIdcode ((shiftL (fromIntegral high) 16) .|. (fromIntegral low))
packetAction (Packet1 Write Snowplow (high:[])) = WriteSnowplow high
packetAction (Packet1 Write HcOptReg (hcopt:[])) =
  let (bramskip,tworound,hccycle) = codeHcOpt hcopt
  in WriteHcOptReg bramskip tworound hccycle
packetAction (Packet1 Write General1 (low:[])) = WriteGeneral1 low
packetAction (Packet1 Write General2 (high:[])) = WriteGeneral2 high
packetAction (Packet1 Write ModeReg (mode:[])) =
  let (newmode,bootmode) = codeMode mode
  in WriteModeReg newmode bootmode
packetAction (Packet1 Write PuGwe (cycle:[])) = WritePuGwe cycle
packetAction (Packet1 Write PuGts (cycle:[])) = WritePuGts cycle
packetAction (Packet1 Write MfwrReg _) = WriteMfwr
packetAction (Packet1 Write CclkFreq (freq:[])) = WriteCclkFreq freq
packetAction (Packet1 Write SeuOpt (seuopt:[])) = WriteSeuOpt seuopt
packetAction (Packet1 Write ExpSign (high:low:[])) = WriteExpSign ((shiftL (fromIntegral high) 16) .|. (fromIntegral low))
packetAction _ = undefined

actionCode :: Action -> [Word16]
actionCode = packetCode.actionPacket 


data ConfFrame = ConfFrame Word32 [Word16] deriving (Read, Show)
instance Eq ConfFrame where
  (==) (ConfFrame x _) (ConfFrame y _) = (==) x y
instance Ord ConfFrame where
  compare (ConfFrame x _) (ConfFrame y _) = compare x y
 
fillExtFrames :: [Action] -> [ConfFrame] -> [Action]
fillExtFrames orig frames = 
  let extFiller frames acc (WriteFdriExt from to) =
        let takeFrames = (takeWhile ((>=) (ConfFrame to []))) $ dropWhile ((>) (ConfFrame from [])) frames
            concatFrames = (concatMap (\(ConfFrame _ frm) -> frm) takeFrames) ++ emptyFrame
        in (WriteFdri concatFrames):acc 
      extFiller frames acc (WriteSingleFramesExt from to) =
        let takeFrames = (takeWhile ((>=) (ConfFrame to []))) $ dropWhile ((>) (ConfFrame from [])) frames
            makeWrite acc' (ConfFrame addr frm) = (WriteFdri (frm ++ emptyFrame)):(WriteCmd Wcfg):(WriteFar addr):acc'
        in foldl' makeWrite acc takeFrames
      extFiller _ acc x = x:acc
  in reverse $ foldl' (extFiller (sort $ nub frames)) [] orig


codeGenAct :: [Action] -> [Word16]
codeGenAct = codeGen.(map actionPacket) 

codeGen :: [Packet] -> [Word16]
codeGen = ((++) (replicate 16 0xFFFF)).((:) 0xAA99).(concatMap packetCode)

codeParse :: String -> [Action]
codeParse s =
  let inBits = tail $ dropWhile (/= 0xAA99) $ readRbt s
  in map packetAction $ packets $ readRbt s
