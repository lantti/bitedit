import Control.Monad
import S3Bits
import Data.Word
import Data.Bits

main = do
  r <- getContents
  print (filter (\(ConfFrame _ xs) -> any ((/=) 0) xs) (read r))

