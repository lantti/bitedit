import Control.Monad
import S3Bits
import Data.List.Split
import Data.Word
import Data.Bits
import qualified Data.Text as T
import System.Environment

getFrames :: Packet -> [Word16]
getFrames (Packet1 Write Fdri frms) = frms
getFrames (Packet2 Write Fdri frms) = frms
getFrames _ = []



main = do
  args <- getArgs
  rbt <- readFile (args !! 0)
  putStr $ T.unpack $ T.replace (T.pack "],") (T.pack "],\n") $ T.pack $ show $ map (\(addr,frm) -> ConfFrame addr frm) $ zip frameOrder $ chunksOf 74 $ concatMap getFrames $ packets $ readRbt rbt
