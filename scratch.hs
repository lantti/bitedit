import S3Bits
import Data.Bits
import Text.Printf

isLout :: Packet -> Bool
isLout (Packet1 _ Lout _) = True
isLout (Packet2 _ Lout _) = True
isLout _ = False

getAddr :: Packet -> Int
getAddr (Packet1 Write Lout (high:low:[])) = ((shiftL (fromIntegral high) 16) .|. (fromIntegral low))
getAddr _ = undefined

frmaddrs :: String -> [Int]
frmaddrs s = map getAddr $ filter isLout $ packets $ readRbt s

addrStrs :: [Int] -> [String]
addrStrs = map (printf "0x%08x")


