import Control.Monad
import S3Bits
import Data.Word
import Data.Bits

isLout :: Packet -> Bool
isLout (Packet1 _ Lout _) = True
isLout (Packet2 _ Lout _) = True
isLout _ = False

main = do
  rbt <- getContents
  putStr $ writeRbt "xxx.xxx" "spartan3a" "3s50avq100" "Mon Feb  9 03:58:57 2015" $ codeGen $ filter (not.isLout) $ packets $ readRbt rbt
