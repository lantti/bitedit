module Bitfield where

import Data.Bits

class Bitfield t where
  fields :: t -> [(Int, Int)]
  toBits :: t -> Int
  fromBits :: Int -> t

--  toBits (t a) = (shiftL (fromEnum a) (snd fields)) 
--  fromBits b = f (toEnum ((shiftR b (snd field1)) .&. (foldl setbit 0 [(fst field1)..(snd field1)])))

data This = T1 | T2 | T3 deriving (Show, Enum)
data That = H1 | H2 | H3 deriving (Show, Enum)
data Field = Field This That

instance Bitfield Field where
  fields _ = [(14,3)]
  toBits _ = 5
  fromBits _ = Field T1 H2
