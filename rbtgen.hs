import Control.Monad
import S3Bits
import Data.Word
import Data.Bits
import System.Environment

bits16 :: (Integral a, Bits a) => a -> String
bits16 x = concatMap (show.toInteger.((.&.) 1).rotateR x) [15,14..0]

main = do
  args <- getArgs
  conf <- readFile (args !! 0)
  frames <- readFile (args !! 1)
  r <- return $ codeGenAct $ fillExtFrames (read conf) (read frames)
  putStr $ writeRbt "xxx.xxx" "spartan3a" "3s50avq100" "Mon Feb  9 03:58:57 2015" r

